package GUI_2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class CreateTreningsDialog extends JDialog{
	
	private MainFrame parent;
	private JPanel panelMain;
	private CreateTreningsDialog mainDialog;
	
	private String yourTreningPlanName;
	private String rootPath;
	private String filePath;
	private String sep = File.separator;
	private String excercisesPath;
	private String trainingRoot;
	private String trainingsPath;
	
	private File excercisesFile;
	private File training;
	
	// Labels
	private JLabel labelChooseMuscle, labelChooseExcercise, firstLabel ;
	private JLabel yourTrainingPlanTitleLabel;
	private JLabel choosenExcerciseLabel;
	private String excerciseName = "";
	private JLabel setsLabel;
	
	//Buttons
	private JButton provideTreningDetails;
	private JButton addNewExcercise;
	private JButton viewPlan;
	private JButton addSetButton;
	private JButton addExcerciseToTrening;
	private JButton resetAllSets;
	private JButton resetExcercise;
	private JButton saveTrainingToFile;
	private JButton exit;
	private int buttonYlocation = 455;
	private int buttonLine2Ylocation = 490;
	private int buttonYlocationPlan = 345;
	//Combo boxes	
	private JComboBox bodyPartList  ;
	private DefaultComboBoxModel bodyPartsModel;
	
	private String bPart = "";
	private BodyParts[]bodyParts;
	
//	private DefaultComboBoxModel numBoxModel;
	
	private TreningAreaView selectedTrainingView;
	
	private JList excerciseList;
	private DefaultListModel listModel;
	private JScrollPane listScroll;
	
	
	
	private PopUpPanel popUpPanel;
	
	private int noOfSeries;
	private static int mainPanelHeight;
	private static int mainDialogHeight;
	
	private int counter = 0;
	
	private int[] dialogLocation;
	private Set set = null;
	private ArrayList<Set>listOfSets = new ArrayList<Set>();
	private ArrayList<String>trainings = new ArrayList<String>();
	private int excerciseCounter = 0;
	
	String numX = "";
	String repX = "";
	String weightX = "";
	String hashnum = "";
	int setHashCode = 0;
	
	// main constructor
	public CreateTreningsDialog(){
				
	// setting panel
		mainPanelHeight = 425;
		mainDialogHeight = 425;
		panelMain = new JPanel();		
		panelMain.setSize(480, mainPanelHeight);		
		panelMain.setBorder(BorderFactory.createLineBorder(Color.BLACK)); // remove later
		panelMain.setLayout(null);
		
		setTitle("Create your own Training Plan");
		setContentPane(panelMain);
		setSize(new Dimension(505, mainPanelHeight));
		// setting dialog's modality
		setModal(false); 
		
		// setting location of dialog in relation to main frame
		parent = new MainFrame();				
		dialogLocation = parent.setParentLocation();
		setLocation(dialogLocation[0] + 75, dialogLocation[1] + 70);
		
		popUpPanel = new PopUpPanel();
		popUpPanel.setVisible(false);
		panelMain.add(popUpPanel);
		
		hashnum = "" + popUpPanel.hashCode();
		System.out.println("Hash for popUpPanel is " + hashnum);
		
		
	// setting panel contents 		
		
		// Paths for data files
		rootPath = new File("").getAbsolutePath();
		excercisesPath = rootPath + sep + "src" + sep + "excercises"  + sep;
		trainingRoot = rootPath + sep + "src" + sep + "trenings"  + sep;
		trainingsPath  = "";
		
		
	// Buttons initialized
		
		// this button with label ask for your name of new training plan and display it 
		provideTreningDetails = new JButton("Name you training plan");
		provideTreningDetails.setBounds(10, 11, 157, 23);
		panelMain.add(provideTreningDetails);
				
		// this combo box lets user pick body part for training				
		bodyPartList = new JComboBox();
		bodyPartList.setBounds(127, 81, 156, 23);
		bodyPartList.setEnabled(false);
		panelMain.add(bodyPartList);
		
		
		/* this button opens small dialog that allows user to add new exercise which is not on the list	*/
		addNewExcercise = new JButton("Add new excercise");
		addNewExcercise.setBounds(127, 111, 157, 23);
		addNewExcercise.setEnabled(false);
		panelMain.add(addNewExcercise);
		
		
		listModel = new DefaultListModel();
		
		// this button creates set row
		addSetButton = new JButton(" Add new Set ");
		addSetButton.setBounds(58, 345, 104, 20);
		addSetButton.setEnabled(false);	
		panelMain.add(addSetButton);		
		
		addExcerciseToTrening = new JButton(" Add excercise ");
		addExcerciseToTrening.setBounds(170, buttonYlocation, 150, 20);
		addExcerciseToTrening.setEnabled(false);
		addExcerciseToTrening.setVisible(true);
		
		viewPlan = new JButton(" View plan ");
		viewPlan.setBounds(330, buttonYlocationPlan, 150, 20);	
		viewPlan.setVisible(true);
		
		resetAllSets = new JButton(" Reset all sets ");
		resetAllSets.setBounds(10, buttonYlocation, 150, 20);
		resetAllSets.setEnabled(false);		
		
		resetExcercise = new JButton(" Reset excercise ");
		resetExcercise.setBounds(330, buttonYlocation, 150, 20);
		resetExcercise.setEnabled(false);
		
		saveTrainingToFile = new JButton(" Save training ");
		saveTrainingToFile.setBounds(10, buttonLine2Ylocation, 150, 20);
		saveTrainingToFile.setEnabled(false);
		
		exit = new JButton(" Exit ");
		exit.setBounds(330, buttonLine2Ylocation, 150, 20);
		exit.setEnabled(false);
		
	// Labels	
		
		// this label asks user to choose number of series for choosen excercise
		firstLabel = new JLabel("Pick one body part and choose your favorite excercise");
		firstLabel.setBounds(10, 46, 450, 23);
		panelMain.add(firstLabel);
		
		yourTrainingPlanTitleLabel = new JLabel("");
		yourTrainingPlanTitleLabel.setBounds(180, 11, 200, 23);
		panelMain.add(yourTrainingPlanTitleLabel);
				
		labelChooseMuscle = new JLabel("Body parts");
		labelChooseMuscle.setBounds(10, 81, 95, 14);
		panelMain.add(labelChooseMuscle);
				
		labelChooseExcercise = new JLabel("Excercises");
		labelChooseExcercise.setBounds(10, 111, 95, 14);
		panelMain.add(labelChooseExcercise);
					
		choosenExcerciseLabel = new JLabel("");
		choosenExcerciseLabel.setBounds(10, 316, 500, 14);
		choosenExcerciseLabel.setEnabled(false);
		panelMain.add(choosenExcerciseLabel);
		
		setsLabel = new JLabel("Sets");
		setsLabel.setBounds(20, 345, 41, 14);
		panelMain.add(setsLabel);	
		
		
		// this block will crate training object
		provideTreningDetails.addActionListener(new ActionListener(){			
			
			public void actionPerformed(ActionEvent e){				
				// first make title
				yourTreningPlanName = JOptionPane.showInputDialog( panelMain, "Please provide title for your Training Plan");
				
				// validate entry
				if(!yourTreningPlanName.equals("")){				
					// set for label
					yourTrainingPlanTitleLabel.setText(yourTreningPlanName);					
					// now time to create text file to store training	
					trainingsPath = trainingRoot + yourTreningPlanName + ".txt";
					
					training = new File(trainingsPath);					
						if(!training.exists()){
							try {
								training.createNewFile();
								// this will provide some information header for training
								DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
								String date = df.format(new Date());
								String header = yourTreningPlanName + " created on: " + date;
								
								
								PrintWriter addExce;
								try {
									addExce = new PrintWriter(new BufferedWriter(new FileWriter(trainingsPath,true)));					
									addExce.println(header);
									addExce.close();						
								} catch (IOException e1) {
								}					
							
							} catch (IOException e1) {}
						}else{
							JOptionPane.showMessageDialog( panelMain, "Training with this name already exists");
						}				
					
					bodyPartList.setEnabled(true);
//					addNewExcercise.setEnabled(true);	
				}else{
					JOptionPane.showMessageDialog( panelMain, "This is not valid name, try again");
				}
			}
		});
		
		
		
		// this part will fill combo box with available body parts to work with		
		bodyPartsModel = new DefaultComboBoxModel();
		bodyPartsModel.addElement(""); //default blank element		
		
		bodyParts = BodyParts.values();
		
			for(BodyParts bodyPart: bodyParts){
				bodyPartsModel.addElement(bodyPart.toString());
			}				
		bodyPartList.setModel(bodyPartsModel);
		
		bodyPartList.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
					
			if(!(training == null)){				
				bPart = (String) bodyPartList.getSelectedItem();				
				System.out.println("selected item is " + bPart);		
						
				switch(bPart){					
					case"":
						listModel.removeAllElements();
						addNewExcercise.setEnabled(false);
						choosenExcerciseLabel.setText("");
						break;
					case "Chest":
						readExcercises();
						addNewExcercise.setEnabled(true);
						break;						
					case "Sholders":
						readExcercises();
						addNewExcercise.setEnabled(true);
						break;													
					case "Back":
						readExcercises();
						addNewExcercise.setEnabled(true);
						break;						
					case "Biceps":
						readExcercises();
						addNewExcercise.setEnabled(true);
						break;						
					case "Triceps":
						readExcercises();
						addNewExcercise.setEnabled(true);
						break;						
					case "Legs":
						readExcercises();
						addNewExcercise.setEnabled(true);
						break;
					}
				}else{ 	JOptionPane.showMessageDialog(panelMain, "Please name your training first"); 
				}	
			}
		});
				
		/* this button opens small dialog that allows user to add new exercise which is not on the list	*/		
		addNewExcercise.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e){				
				
				if(!(training == null)){
					
					// name of new excercise
					String newExcercise = JOptionPane.showInputDialog(panelMain, "enter new excercise");
					System.out.println("this is new excercise " + newExcercise);					
					
					// name of part selected from list
					bPart = (String) bodyPartList.getSelectedItem();
					System.out.println("selcted body part " + bPart);
					// name of file to save it
					String name = bPart + ".txt";					
					addExcercise(newExcercise, name);		
					readExcercises();				
				}else{ 	JOptionPane.showMessageDialog(panelMain, "Please name your training first"); }		
			}
		});
		
				
		// this list will display all exercises loaded from file related to body part chosen in bodyPartList comboBox
		excerciseList = new JList(listModel);		
		listScroll = new JScrollPane(excerciseList);
		listScroll.setBorder(new LineBorder(new Color(0, 0, 0)));
		listScroll.setBounds(10, 140, 470, 164);
		panelMain.add(listScroll);
		
		// now when one of excercises are picked it will be displayed beside Selected Training:
		excerciseList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				
				
				int selected[] = excerciseList.getSelectedIndices( );					
					 for (int i = 0; i < selected.length; i++) {
						 excerciseName =  (String)excerciseList.getModel( ).getElementAt(selected[0]);
						 choosenExcerciseLabel.setText("Selected Training: " + excerciseName);
					 }		        
		        choosenExcerciseLabel.setEnabled(true);		        
		        addSetButton.setEnabled(true);
		    	addExcerciseToTrening.setEnabled(true);		    
		    	resetAllSets.setEnabled(true);
		    	resetExcercise.setEnabled(true);
		    	saveTrainingToFile.setEnabled(false);
		    	exit.setEnabled(true);
			}
		});
		
		addSetButton.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
			
			if(!(training == null)){	
					if(!(choosenExcerciseLabel.equals("")) || !(choosenExcerciseLabel.equals("")) ){						
						if(counter == 0 || set == null){
							
							if(counter == 0){
								mainDialogHeight += 110;
								buttonYlocation = 455;
								buttonLine2Ylocation = 490;
							}
							System.out.println("counter is " + counter);
							System.out.println("counter is " + counter);
							counter++;
							popUpPanel.setVisible(true);
							popUpPanel.creteLableRow(counter);
							
							setSize(new Dimension(505, mainDialogHeight ));							
							panelMain.add(addExcerciseToTrening);
							panelMain.add(viewPlan);
							panelMain.add(resetAllSets);
							panelMain.add(resetExcercise);
							panelMain.add(saveTrainingToFile);
							panelMain.add(exit);
							
							addExcerciseToTrening.setVisible(true);
							resetAllSets.setVisible(true);
							resetExcercise.setVisible(true);
							addExcerciseToTrening.setEnabled(true);
							saveTrainingToFile.setVisible(true);
							
							addExcerciseToTrening.setBounds(170, buttonYlocation, 150, 20);
							resetAllSets.setBounds(10, buttonYlocation, 150, 20);
							resetExcercise.setBounds(330, buttonYlocation, 150, 20);
							saveTrainingToFile.setBounds(10, buttonLine2Ylocation, 150, 20);
							exit.setBounds(330, buttonLine2Ylocation, 150, 20);
							
							set = new Set();
							
						}else{
							
							if(counter >= 1){	
								if(!popUpPanel.repsValueDisplayedString.equals("") && !popUpPanel.weightsValueDisplayedString.equals("")){						
									System.out.println("counter is " + counter);
									set.setNum("" + counter);
									set.setReps(popUpPanel.repsValueDisplayedString);
									set.setWeight(popUpPanel.weightsValueDisplayedString);
									listOfSets.add(set);
									
									numX = set.num;	
									repX = set.reps;
									weightX = set.weight;
//									System.out.println("Object filled " + numX + ", " + repX + ", " + weightX );
									
									popUpPanel.repsValueDisplayedString = "";	
									popUpPanel.weightsValueDisplayedString = "";
									
									counter++;
									popUpPanel.creteLableRow(counter);
									popUpPanel.setVisible(true);
									setSize(new Dimension(505, mainDialogHeight ));
									
									buttonYlocation += 30;	
									buttonLine2Ylocation += 30;
									addExcerciseToTrening.setBounds(170, buttonYlocation, 150, 20);
									addExcerciseToTrening.setEnabled(true);
									resetAllSets.setBounds(10, buttonYlocation, 150, 20);
									resetExcercise.setBounds(330, buttonYlocation, 150, 20);
									saveTrainingToFile.setBounds(10, buttonLine2Ylocation, 150, 20);
									exit.setBounds(330, buttonLine2Ylocation, 150, 20);
									saveTrainingToFile.setEnabled(false);
									set = new Set();

								}else{
									JOptionPane.showMessageDialog(parent, "Please provide the reps and weight first");}				
							}else{}					
						}
						
						provideTreningDetails.setEnabled(false);
						addNewExcercise.setEnabled(false);
						bodyPartList.setEnabled(false);
						excerciseList.setEnabled(false);
						
					}else{ JOptionPane.showMessageDialog(parent, "Please choose excercise first");}
				}else{ 	JOptionPane.showMessageDialog(parent, "Please name your training first"); }					
			}
		});
		
		
		addExcerciseToTrening.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {				
				
				if(!popUpPanel.repsValueDisplayedString.equals("") && !popUpPanel.weightsValueDisplayedString.equals("")){					
				
					// set last entred data to object and add them to array
					set.setNum("" + counter);
					System.out.println("Counter is - " + counter);
					set.setReps(popUpPanel.repsValueDisplayedString);
					set.setWeight(popUpPanel.weightsValueDisplayedString);
					listOfSets.add(set); 
					
					// add excercise to training
					String setline = "";					
									
						trainings.add(excerciseName);					
					
						for(Set s: listOfSets){
							setline = s.num + "," + s.reps + "," + s.weight;
							trainings.add(setline);
							System.out.println("listOfSets before " + s);
						}
					
					// reset sets
					counter = 0;
					listOfSets.removeAll(listOfSets);
					popUpPanel.repsValueDisplayedString = "";
					popUpPanel.weightsValueDisplayedString = "";	
					set = null;					
					popUpPanel.removeExcercise();
					
					// resize panels
					mainDialogHeight = 465;
					mainPanelHeight = 465;
					setSize(new Dimension(505, mainDialogHeight ));
					
//					buttonYlocation = 415;
					buttonLine2Ylocation = 385;
					addExcerciseToTrening.setEnabled(false);
					addExcerciseToTrening.setVisible(false);
					resetExcercise.setEnabled(false);
					resetExcercise.setVisible(false);
					resetAllSets.setEnabled(false);
					resetAllSets.setVisible(false);
					saveTrainingToFile.setEnabled(true);
					
					
					addExcerciseToTrening.setBounds(10, buttonYlocation, 150, 20);
					resetAllSets.setBounds(170, buttonYlocation, 150, 20);
					resetExcercise.setBounds(330, buttonYlocation, 150, 20);
					saveTrainingToFile.setBounds(10, buttonLine2Ylocation, 150, 20);
					exit.setBounds(330, buttonLine2Ylocation, 150, 20);
					
					
					popUpPanel.setVisible(false);
					addSetButton.setEnabled(false);
					provideTreningDetails.setEnabled(true);
					addNewExcercise.setEnabled(true);
					bodyPartList.setEnabled(true);
					excerciseList.setEnabled(true);
					choosenExcerciseLabel.setText("");
					
					mainDialogHeight = 425;
					mainPanelHeight = 425;
					
				}else{
					JOptionPane.showMessageDialog(parent, "Please provide the reps and weight first");}	
			}			
		});
	
		
		saveTrainingToFile.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {				
				
				excerciseCounter++;
				System.out.println("\nnow we saving"); //test
				resetAllSets.setEnabled(false);
				saveTrainingToFile.setEnabled(false);
				addExcerciseToTrening.setEnabled(false);				
				
				PrintWriter addExce;
				try {
					addExce = new PrintWriter(new FileWriter(trainingsPath,true));				 					
					for(String s: trainings) {
						addExce.println( s );
						System.out.print(s);
				    }
					addExce.close();					
				} catch (IOException e1) {}
				
				trainings.removeAll(trainings);
				System.out.println("Size of trainig is " + trainings.size());
			}
		});
		
		
		/* this button is responsible for a feature with lets 
		 * user to reset all sets he made so far but it 
		 * lets hem stay working on the same exercise and training
		 */
		resetAllSets.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				
				popUpPanel.removeRows();
				
				counter = 0;
				listOfSets.removeAll(listOfSets);
				popUpPanel.repsValueDisplayedString = "";
				popUpPanel.weightsValueDisplayedString = "";
				
				// resize panels
				mainDialogHeight = 425;
				mainPanelHeight = 425;				
				buttonYlocation = 455;
				buttonLine2Ylocation = 490;
				addExcerciseToTrening.setEnabled(true);
				saveTrainingToFile.setEnabled(false);
				addExcerciseToTrening.setBounds(10, buttonYlocation, 150, 20);
				resetAllSets.setBounds(170, buttonYlocation, 150, 20);
				resetExcercise.setBounds(330, buttonYlocation, 150, 20);						
				saveTrainingToFile.setBounds(10, buttonLine2Ylocation, 150, 20);
				exit.setBounds(330, buttonLine2Ylocation, 150, 20);
				counter++;
				set = new Set();
				
				popUpPanel.setVisible(true);
				popUpPanel.creteLableRow(counter);
				mainDialogHeight += 110;				
				setSize(new Dimension(505, mainDialogHeight ));
			
			}
		});
		
		/* The purpose of this button is to give user an option to 
		 * to remove sets and exercise he works on and let him chose 
		 * new exercise again for the same training		
		 */
		resetExcercise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
								
				set = null;
				
				popUpPanel.removeExcercise();
				counter = 0;
				listOfSets.removeAll(listOfSets);
				
				// resize panels
				mainDialogHeight = 425;
				mainPanelHeight = 425;				
				buttonYlocation = 455;
				buttonLine2Ylocation = 490;
				addExcerciseToTrening.setBounds(10, buttonYlocation, 150, 20);
				resetAllSets.setBounds(170, buttonYlocation, 150, 20);
				resetExcercise.setBounds(330, buttonYlocation, 150, 20);
				saveTrainingToFile.setBounds(10, buttonLine2Ylocation, 150, 20);
				
				setSize(new Dimension(505, mainDialogHeight ));
				
				popUpPanel.setVisible(false);
				addSetButton.setEnabled(false);
				provideTreningDetails.setEnabled(true);
				addNewExcercise.setEnabled(true);
				bodyPartList.setEnabled(true);
				excerciseList.setEnabled(true);
				choosenExcerciseLabel.setText("");
				
			}
		});
	
		
		exit.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {				
				if(excerciseCounter == 0){
					JOptionPane.showConfirmDialog(panelMain, " This training is empty.\nDo you want to keep it?");
				}
				
			}
		});
		
		
		viewPlan.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {				
				String fileContens = "\n"; 
				String line = "";
				BufferedReader treningReader;					
					
				System.out.println("Trenning is " + trainingsPath);	
				
				for(String s: trainings) {
					fileContens += s + "\n";	
					System.out.println(fileContens);
			    }				
				selectedTrainingView = null;					
					// this condition checks if training file isn't empty				
					if(!fileContens.equals("")){
//						selectedTrainingView = new TreningAreaView(trainingsPath, fileContens, mainDialog);
						selectedTrainingView.setVisible(true);
					}
					else{
						JOptionPane.showMessageDialog(null, "File is empty");						
					}				
			}
		});
		
//end of constructor
	}
	
	
		//this method will provide functionality for adding new exercise to data file
		// it will be called by addNewExcercise button action listener
	public void addExcercise(String excercise, String name){		
		
		try {			
			if (!excercise.equals("")){						
				PrintWriter addExce = new PrintWriter(new BufferedWriter(new FileWriter(new File(excercisesPath + name),true)));
				addExce.println(excercise);
				addExce.close();
			}else{
				JOptionPane.showMessageDialog(null, "This is not valid name, try again");
			}
		}
		 catch (IOException e1) {
			e1.printStackTrace();
		}
	}


	
  // this method will read excercise files
	public void readExcercises(){
		
				String line;
				String name = bPart + ".txt";
				String Path = excercisesPath + name;
											
				try {					
					excercisesFile = new File(Path); // path set up
//					System.out.println("this path is " + Path );
					
					if(excercisesFile.exists()){									
//						System.out.println("this file exists " + excercisesFile.exists());
						
						listModel.removeAllElements();	// to avoid duplication								
						BufferedReader excerciseReader1 = new BufferedReader(new FileReader(excercisesFile));
						
						line = excerciseReader1.readLine();									
						if(line != null){
							
							listModel.addElement(line);
							while((line = excerciseReader1.readLine()) != null){						
								
								listModel.addElement(line);											
//								System.out.println("line after is " + line);										
							}
						}else{
							JOptionPane.showMessageDialog(null, "File is empty. Please add your excercises");
							}
					}
					else{
						excercisesFile.createNewFile();
//						System.out.println("File has been created " + name);
						JOptionPane.showMessageDialog(null, "File is empty. Please add your excercises");
					}
				} catch (Exception e1) {								
				}
				
			
		
	}
	
	
	
  // this inner class will create special panel for holding rows of set
	class PopUpPanel extends JPanel{
		
		//Header labels displays
		JLabel numberLabel;
		JLabel repsLabel;
		JLabel weightsLabel;
		
		//Controls for setting 
		JLabel setlabel;
		JComboBox repsBox;
		JComboBox weightsBox;
		
		//Header labels displays
		JLabel numberLabelDisplayed;
		JLabel repsLabelDisplayed;
		JLabel weightsLabelDisplayed;
		
		//Strings for training values
		String numberValueDisplayedString = "";
		String repsValueDisplayedString = "";
		String weightsValueDisplayedString = "";
		
		//Displayed Value
		JLabel numberValueDisplayed;
		JLabel repsValueDisplayed;
		JLabel weightsValueDisplayed;
		
		// initial size for components
		int popUpPanelHeight = 40;		
		
		int lblHeight = 14;
		int lblWeight = 25;
		
		int lblPosX = 10;
		int lblPosY = 40;
		
		int repsBoxHeight = 14;
		int repsBoxWeight = 45;
		
		int repsBoxPosX = 37;
		int repsBoxPosY = 40;
		
		int weightBoxHeight = 14;
		int weightBoxWeight = 55;
		
		int weightBoxPosX = 94;
		int weightBoxPosY = 40;
		
		int lblValueHeight = 14;
		int lblValueWeight = 25;
		
		int lblValuePosX = 215;
		int lblValuePosY = 40;
		
		int repsValueHeight = 14;
		int repsValueWeight = 70;
		
		int repsValuePosX = 280;
		int repsValuePosY = 40;
		
		int weightValueHeight = 14;
		int weightValueWeight = 70;
		
		int weightValuePosX = 340;
		int weightValuePosY = 40;
		
		
		
		public PopUpPanel() {
			
//				setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				
			setLocation(10, 368);
			mainPanelHeight = 425;
			mainDialogHeight = 425;				
			setSize(222, popUpPanelHeight);
			setBorder(BorderFactory.createLineBorder(Color.RED));
			setLayout(null);
				
			numberLabel = new JLabel("No.");
			numberLabel.setBounds(10, 11, 25, 14);
			
			repsLabel = new JLabel("Repetitions");
			repsLabel.setBounds(37, 11, 70, 14);
					
			weightsLabel = new JLabel("Weights");
			weightsLabel.setBounds(110, 11, 46, 14);
				
			numberLabelDisplayed = new JLabel("No.");
			numberLabelDisplayed.setBounds(200, 11, 25, 14);
				
			repsLabelDisplayed = new JLabel("Repetitions");
			repsLabelDisplayed.setBounds(238, 11, 70, 14);
				
			weightsLabelDisplayed = new JLabel("Weights");
			weightsLabelDisplayed.setBounds(310, 11, 46, 14);
				
			
			// header components added	
			add(numberLabel);
			add(repsLabel);
			add(weightsLabel); 
				
			add(numberLabelDisplayed);
			add(repsLabelDisplayed);
			add(weightsLabelDisplayed);
				
			}
//		public void setHeightOfPopUpPanel(int height){
//			this.popUpPanelHeight = height;
//		}
		public void removeExcercise(){
			
			popUpPanel.removeAll();				
			popUpPanel.revalidate();
			popUpPanel.repaint();
			
			numberValueDisplayedString = "";
			repsValueDisplayedString = "";
			weightsValueDisplayedString = "";
			
			
			add(numberLabel);
			add(repsLabel);
			add(weightsLabel); 
			
			 
			add(numberLabelDisplayed);
			add(repsLabelDisplayed);
			add(weightsLabelDisplayed);
			
			this.lblPosY = 40;
			this.repsBoxPosY = 40;
			this.weightBoxPosY = 40;
			
			this.lblValuePosY = 40;
			this.repsValuePosY = 40;
			this.weightValuePosY = 40;
			

			
			this.popUpPanelHeight = 40;
			setSize(470, popUpPanelHeight);
						
			// test to remove
			int numOfComponents = popUpPanel.getComponentCount();
//			System.out.println("\n\nPanel has " + numOfComponents + " labels ");
			
		}
		
		public void removeRows() {
			
			// test to remove
			int numOfComponents = popUpPanel.getComponentCount();
//			System.out.println("\n\nPanel has " + numOfComponents + " labels ");
			
				popUpPanel.removeAll();				
				popUpPanel.revalidate();
				popUpPanel.repaint();
				
				add(numberLabel);
				add(repsLabel);
				add(weightsLabel); 
				
				 
				add(numberLabelDisplayed);
				add(repsLabelDisplayed);
				add(weightsLabelDisplayed);
				
				this.lblPosY = 40;
				this.repsBoxPosY = 40;
				this.weightBoxPosY = 40;
				
				this.lblValuePosY = 40;
				this.repsValuePosY = 40;
				this.weightValuePosY = 40;
				
//				System.out.println("exit row positions = " + lblPosY + ", "+ repsBoxPosY + ", "+ weightBoxPosY);
				
				this.popUpPanelHeight = 40;
				setSize(495, popUpPanelHeight);
				
				// test to remove
				numOfComponents = popUpPanel.getComponentCount();
//				System.out.println("Panel has " + numOfComponents + " labels and size of " + popUpPanelHeight);	
				
		}

		public void creteLableRow(int num){				
			
//			System.out.println("I entering");
			// adjusting containers 
			this.popUpPanelHeight += 31;
			CreateTreningsDialog.mainPanelHeight += 31;
			CreateTreningsDialog.mainDialogHeight += 31;
			setSize(470, popUpPanelHeight);			
													
			setlabel = new JLabel(num + ".");
			setlabel.setBounds(lblPosX, lblPosY, lblWeight, lblHeight);
			add(setlabel);
			lblPosY += 31;			

			repsBox = new JComboBox();					
			DefaultComboBoxModel repsBoxModel = new DefaultComboBoxModel();					
				for(int i = 1; i <= 40; i++){
						repsBoxModel.addElement(""+i);					
				}					
			repsBox.setModel(repsBoxModel);					
			repsBox.setBounds(repsBoxPosX, repsBoxPosY, repsBoxWeight, repsBoxHeight);					
			add(repsBox);					
			repsBoxPosY += 31;
			
			repsBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
//					int selected = (Integer) repsBox.getSelectedItem();?
					repsValueDisplayedString = (String) repsBox.getSelectedItem();						
//					System.out.println("selected  " + selected);							
					repsValueDisplayed.setText("" + repsValueDisplayedString);	
//					System.out.println("\n Reps is " + repsValueDisplayedString);
				}
					
			});	
					
			// ok		
			weightsBox = new JComboBox();
		    DefaultComboBoxModel weightsBoxModel = new DefaultComboBoxModel();			 
		    	for(double j = 0; j <= 300; j += 0.50){
		    		weightsBoxModel.addElement(""+j);
					}					
			weightsBox.setModel(weightsBoxModel);
			weightsBox.setBounds(weightBoxPosX, weightBoxPosY, weightBoxWeight, weightBoxHeight);
			add(weightsBox);
			weightBoxPosY += 31;					
					
			weightsBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					weightsValueDisplayedString =  (String) weightsBox.getSelectedItem();
					weightsValueDisplayed.setText("" + weightsValueDisplayedString);
//					System.out.println("\n Weight is " + weightsValueDisplayedString);		
				}
					
			});	
			numberValueDisplayedString = num + "";
			
			numberValueDisplayed = new JLabel(numberValueDisplayedString);
			numberValueDisplayed.setBounds(lblValuePosX, lblValuePosY, lblValueWeight, lblValueHeight);
			add(numberValueDisplayed);
			lblValuePosY += 31;
			
//				System.out.print(numberValueDisplayedString + "/" );
			
			repsValueDisplayed = new JLabel();
			repsValueDisplayed.setBounds(repsValuePosX, repsValuePosY, repsValueWeight, repsValueHeight);
			add(repsValueDisplayed);
			repsValuePosY += 31;
			
//				System.out.print(repsValueDisplayedString + "/" );
					
			weightsValueDisplayed = new JLabel();
			weightsValueDisplayed.setBounds(weightValuePosX, weightValuePosY, weightValueWeight, weightValueHeight);
			add(weightsValueDisplayed);
			weightValuePosY += 31;
			
//				System.out.println(weightsValueDisplayedString);
					
			// test to remove
			int numOfComponents = popUpPanel.getComponentCount();
			
			
//			System.out.println("Panel has " + numOfComponents + " labels");
//			System.out.println("First row positions = " + lblPosY + ", "+ repsBoxPosY + ", "+ weightBoxPosY);
//			System.out.println("I am exiting");
//			System.out.println("next  row positions = " + lblPosY + ", "+ repsBoxPosY + ", "+ weightBoxPosY);	
		}
	}
	
	private class Set{		
		private String num;
		private String reps;
		private String weight;
		
		public Set(){			
		}		
		public Set(String name, String reps, String weight) {
			super();
			this.num = num;
			this.reps = reps;
			this.weight = weight;
		}
		public void setNum(String name) {
			this.num = name;
		}
		public void setReps(String reps) {
			this.reps = reps;
		}
		public void setWeight(String weight) {
			this.weight = weight;
		}
		public String getNum() {
			return num;
		}
		public String getReps() {
			return reps;
		}
		public String getWeight() {
			return weight;
		}
	}
	
}
		
