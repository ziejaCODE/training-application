package GUI_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

class TreningView extends JDialog{
	
	private JTextArea treningView = new JTextArea();
	private JScrollPane textScroll;
	
	private String trainingPath;

	
	
	// constructor takes name of training as title and content of the file
	public TreningView(String treningTitlePath){		
		
		System.out.println("What is " + treningTitlePath);
		trainingPath = treningTitlePath;		
		treningView = new JTextArea();
		setTitle("Training for Today");		
		treningView.setText(setTrainingContent(trainingPath));		
		textScroll = new JScrollPane(treningView);
		
		setContentPane(textScroll);
		setSize(500, 600);
		setLocationRelativeTo(null);
		
	}
	
	/* 
	 * This method will take path for the training and read it 
	 * contents and display it on text area
	 */
	public String setTrainingContent(String treningPath){		
		
		String contens = "";
		String line = "";
		BufferedReader treningReader;
		
		if(treningPath == null){
			System.out.println("we have null");
		}
		
		try {						
			treningReader = new BufferedReader(new FileReader(treningPath));
			
			while((line = treningReader.readLine()) != null){							
				contens += line + "\n";	
//				System.out.println(contens);//test
			}
			    
		} catch (IOException e1) {
			System.out.println("Some problem with the file");
			e1.printStackTrace();
		}
		
		if(!contens.equals("")){
			return contens;
		}
		return null;		
	}
	
	
}







