package GUI_2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ViewTreningsDialog extends JDialog{
		
	private JPanel dialogPanel;
	private JList list;
	
	private String filePath;
	private String relativePath;
	private String rootPath;
	private String sep = File.separator;
	
	private File treningFile;
	private String[] treningsTitles;	
	private String treningTitle;
	
	private JScrollPane listScroll;
	
	private JLabel welcomeLabel;
	private JLabel lblInstruction1;
	private JLabel lblInstruction2;
	private JLabel lblSelectedTraining;
	
	private JButton loadTrainingButton;
	private JButton exitButton;
	private JButton viewTreningButton;
	private TreningAreaView selectedTrainingView;
	
	private int[] dialogLocation;
	private MainFrame parent;
	
	private LoginDialog logDial;
	
	private UserStatus userStatus;
	private String ustat;
	
	private String choosenDay;
	
	private JComboBox daysOfTheWeekComBox  ;
	private DefaultComboBoxModel daysOfTheWeekComBoxModel;
	private WeekDays[]days;
	
	public ViewTreningsDialog() {
		
		// setting panel
		dialogPanel = new JPanel();
		dialogPanel.setSize(450, 430);
		dialogPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
		dialogPanel.setLayout(null);
		
		// setting dialog's modality
		setModal(false);		
		
		// information labels
		welcomeLabel = new JLabel("This is entire training list");
		welcomeLabel.setBounds(10, 11, 175, 14);
		dialogPanel.add(welcomeLabel);
				
		lblInstruction1 = new JLabel("Choose one of trainings displayed below");
		lblInstruction1.setBounds(10, 36, 280, 14);
		dialogPanel.add(lblInstruction1);
			
		lblInstruction2 = new JLabel("and load it for using");
		lblInstruction2.setBounds(10, 50, 280, 14);
		dialogPanel.add(lblInstruction2);
				
		// this label will display title of training picked in listbox
		lblSelectedTraining = new JLabel("");
		lblSelectedTraining.setBounds(10, 310, 280, 14);
		dialogPanel.add(lblSelectedTraining);
		
		// this combo box lets user pick day of the week for training				
		daysOfTheWeekComBox = new JComboBox();
		daysOfTheWeekComBox.setBounds(320, 310, 95, 23);
		daysOfTheWeekComBox.setEnabled(true);
		
		dialogPanel.add(daysOfTheWeekComBox);
		
		daysOfTheWeekComBoxModel = new DefaultComboBoxModel();
//		daysOfTheWeekComBoxModel.addElement(""); // empty element
		
		days = WeekDays.values();			
		for(WeekDays day: days){
			daysOfTheWeekComBoxModel.addElement(day.toString());
		}				
		daysOfTheWeekComBox.setModel(daysOfTheWeekComBoxModel);
		
		
		daysOfTheWeekComBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String day = (String) daysOfTheWeekComBox.getSelectedItem();				
				switch(day){
				
				case "Monday":						
					choosenDay = "Mon";
					break;										
				case "Tuesday":						
					choosenDay = "Tue";
					break;					
				case "Wednesday":						
					choosenDay = "Wed";
					break;
				case "Thursday":						
					choosenDay = "Thu";
					break;						
				case "Friday":						
					choosenDay = "Fri";
					break;					
				case "Saturday":						
					choosenDay = "Sat";
					break;
				case "Sunday":						
					choosenDay = "Sun";
					break;
				}					
				System.out.println("selected day is " + choosenDay); // test to remove				
			}
		});
		
		
		
		
	/* here is created functionality for taking the titles of all available 
	 * training from trainings folder and load them into treningsTitles array
	 */	
				
		relativePath = sep + "src" + sep + "trenings" + sep ;
		rootPath = new File("").getAbsolutePath();	
		
		filePath = rootPath + relativePath;
		
			// test 
			System.out.println("this is " + filePath);
		
		treningFile = new File(filePath);
		
		if(treningFile.exists()){
			treningsTitles = treningFile.list();
		}
		else{
			JOptionPane.showMessageDialog(this, "There is no file whit this name\n"
					+ "is " + filePath);
		}
		
		
	/*
	 *  this list automatically loads titles of all existing training planes for user 
	 *  into list box
	 */		
		list = new JList(treningsTitles);
		listScroll = new JScrollPane(list);		
		listScroll.setSize(new Dimension(414, 200));		
		listScroll.setLocation(10, 94);		
		
		DefaultListSelectionModel listModel = new DefaultListSelectionModel();
		listModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listModel.setLeadAnchorNotificationEnabled(false);
		list.setSelectionModel(listModel);
		
		
		// this action display name of selected training into dedicated label 
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				
			int selected = list.getSelectedIndex();	
			String selectedTraining = "Selected Training: ";	
				
			treningTitle =  (String)list.getModel( ).getElementAt(selected);	
			lblSelectedTraining.setText(selectedTraining + "  " + treningTitle);
			
			}
		});
	
		
		dialogPanel.add(listScroll);
		
		// user access details
		userStatus = new UserStatus();
		ustat = userStatus.getUsersStatus();
		
		System.out.println("User status is " + ustat);// test to remove

		
 // Bottom area - Buttons	
	
 // 1. loadTrainingButton
		
		/*
		 * this button is responsible for loading the training to personal user area
		 */		
		loadTrainingButton = new JButton("Load training for use");
		loadTrainingButton.setBounds(134, 340, 175, 23);
		dialogPanel.add(loadTrainingButton);
		
		
		
		// this action will send chosen training path and day of the week to perform training 
		loadTrainingButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {				
				
				logDial = new LoginDialog();
				
				String source = filePath + treningTitle;
				
				if(ustat.equalsIgnoreCase("y")){
					
					// 1.  get path for user training file
					String newTrainingPath = logDial.getUserPath() + choosenDay;				
					System.out.println("oto path splitted " + newTrainingPath);
					
					// 2.  create empty file named with day of the week chosen by user					
					File newTraining = new File(newTrainingPath);
					
					if(!newTraining.exists()){
						
						System.out.println("File not exists");
						try {
							newTraining.createNewFile();
							System.out.println("File created");
						} catch (IOException e1) {
						}
						// 3.  copy content of the chosen training file and save into newly created file 
						copyFile(source, newTraining);						
						
					}else{
						System.out.println("File exists, do you want to replace it?"); // replace it with JOptionPane
						
						copyFile(source, newTraining);						
					}					
					System.out.println(filePath + treningTitle + " day " + choosenDay); // test to remove
				
				}else{
					JOptionPane.showMessageDialog(null, "  To save, please log in first");
				}
			}
		});
		
		
 // 2. exitButton
		
		/*
		 * this button will exit training ViewTreningsDialog 
		 */
		exitButton = new JButton("Exit");
		exitButton.setBounds(319, 340, 95, 23);
		dialogPanel.add(exitButton);		
		
		exitButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
 // 3. viewTreningButton
		
		/*
		 * this button create an action with takes title of training file currently selected, 
		 * read its content and display it in the pop up dialog
		 */
		viewTreningButton = new JButton("View Trening");
		viewTreningButton.setBounds(10, 340, 114, 23);
		dialogPanel.add(viewTreningButton);
				
		viewTreningButton.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				
				String fileContens = ""; 
				String line = "";
				BufferedReader treningReader;
					
					try {						
						treningReader = new BufferedReader(new FileReader(filePath + sep + treningTitle));
						while((line = treningReader.readLine()) != null){							
							fileContens += line + "\n";	
						}
						    
					} catch (IOException e1) {
						System.out.println("Some problem with the file");
						e1.printStackTrace();
					}				
				
				TreningAreaView selectedTrainingView = null;
					
					// this condition checks if training file isn't empty				
					if(!fileContens.equals("")){
						selectedTrainingView = new TreningAreaView(treningTitle, fileContens);
						selectedTrainingView.setVisible(true);
					}
					else{
						JOptionPane.showMessageDialog(null, "File is empty");						
					}
			
			}
		});
		
		// setting JDialog host
		setTitle("View all your Training");
		setContentPane(dialogPanel);		
		setSize(new Dimension(dialogPanel.getSize()));
		
		// setting location of dialog in relation to main frame
//		parent = new MainFrame();				
		dialogLocation = new int[2];
//		int x = dialogLocation.x;
//		int y = dialogLocation.y;
		System.out.println("To moja lokacja " + dialogLocation[0] + " " + dialogLocation[1]);		
		setLocation(dialogLocation[0] + 120, dialogLocation[1] + 150);

	}
	// this method copy one file to another
	public void copyFile(String source, File newTraining){
		
		if(true){
			try {
				BufferedReader in = new BufferedReader(new FileReader(new File(source)));							
				BufferedWriter out = new BufferedWriter(new FileWriter(newTraining));
				
				 boolean eof = false;
				 int inChar = 0;
			      do {
			        inChar = in.read();
			        if (inChar != -1) {
			        	System.out.println(inChar); // test to remove
			          out.write(inChar);
			        } else
			          eof = true;
			      } while (!eof);
			      in.close();
			      out.close();
				
			} catch (IOException e1) {
			}
		}
	}
	

	public void setLocation(int[]location){
		
		dialogLocation = location;
		
	}
	
}














