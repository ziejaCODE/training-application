package GUI_2;


/*
 * this class will define users status after
 * login process witch will designate his access
 * inside the application
 */

public class UserStatus {
	
	private static String userStatus = "n";

	public UserStatus() {
	}
	
	public void setUsersStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUsersStatus() {
		return userStatus;
	}

	

}
