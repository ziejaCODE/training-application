package GUI_2;

import java.util.EventObject;

public class LoginEvent extends EventObject{
	
	private String treiningPath;

	public LoginEvent(Object source) {
		super(source);
		
	}
	
	public LoginEvent(Object source, String treiningPath) {
		super(source);
		this.treiningPath = treiningPath;
	}

	public String getTreiningPath() {
		return treiningPath;
	}
	
	

}
