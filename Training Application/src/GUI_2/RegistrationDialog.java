package GUI_2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RegistrationDialog extends JDialog{

	
	private JLabel titleLable;
	private JPanel mainPanel;
	private JTextField registrationBox;
	private JPasswordField passwordBox;
	private JPasswordField passwordRepeadBox;
	private JButton saveButton;
	
	
	public RegistrationDialog(){
		
		// setting panel
		mainPanel = new JPanel();
		mainPanel.setSize(300,250);
		mainPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
				
		//setting panel components
		titleLable = new JLabel("Please provide login details");
		gc.gridx = 0;
		gc.gridy = 0;
		mainPanel.add(titleLable, gc);
				
		registrationBox = new JTextField(15);
		gc.gridx = 0;
		gc.gridy = 1;
		mainPanel.add(registrationBox, gc);
				
		passwordBox = new JPasswordField(15);
		gc.gridx = 0;
		gc.gridy = 2;
		mainPanel.add(passwordBox, gc);
		
		passwordRepeadBox = new JPasswordField(15);
		gc.gridx = 0;
		gc.gridy = 3;
		mainPanel.add(passwordRepeadBox, gc);
		
		saveButton = new JButton("Create account");
		gc.gridx = 0;
		gc.gridy = 4;
		mainPanel.add(saveButton, gc);
		
		// setting dialog
		setTitle("Login dialog");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setContentPane(mainPanel);
		setSize(new Dimension(mainPanel.getSize()));
		setLocationRelativeTo(null);
		
	}
	
	
}
