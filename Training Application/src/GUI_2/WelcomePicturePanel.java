package GUI_2;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class WelcomePicturePanel extends JPanel{

	private String imageIcon;
	private JLabel imageLabel;
	ImageIcon background;
	
	public WelcomePicturePanel(String title, int interspace){
			
		
		setLayout(new BorderLayout());
		
		// border for this panel
		setBorder(BorderFactory.createTitledBorder(title));	
		setBorder(BorderFactory.createCompoundBorder(
						BorderFactory.createEmptyBorder(interspace, 
								                        interspace, 
								                        interspace, 
								                        interspace), 
						BorderFactory.createTitledBorder(title)));
		
		
	
		add(loadPictures(pictureStorage()), BorderLayout.CENTER);
		
	}
		// this method sets picture to label
		public JLabel loadPictures(String image){			
			background = new ImageIcon(getClass().getResource(image));	
			imageLabel = new JLabel(background);
			
			return imageLabel;
	}
		
		// this method randomly picks picture
		public String pictureStorage(){				
			String[]names = {"/images/foto2.jpg","/images/foto3.jpg","/images/foto4.jpg"};
			imageIcon = names[new Random().nextInt(names.length)];
			
			return imageIcon;
		}	
}























