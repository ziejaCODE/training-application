package GUI_2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;


public class MainFrame extends JFrame{

	private LoginDialog loginDialog;	
	private TextPanel textPanel;	
	private WelcomePicturePanel welcomePanel;
	private ApplicationMenu mainMenu;
	private Point parentLocation;
	static int x;
	static int y;
	static int []location;
	
	ViewTreningsDialog viewTrainings;
	
	private String trainingForToday;
	
	
	public MainFrame() throws HeadlessException {
		
		setTitle("Text area presentation");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(700, 845));		
		setLayout(new BorderLayout());
		

		textPanel = new TextPanel();
		welcomePanel = new WelcomePicturePanel("", 5);
				
		mainMenu = new ApplicationMenu();
		add(welcomePanel, BorderLayout.CENTER);
		setJMenuBar(mainMenu);		
		setLocationRelativeTo(null);		
		parentLocation = getLocation();
		
		loginDialog = new LoginDialog();		
		loginDialog.setEntryPaneListener(new LoginListener(){
			public void loginEventOccured(LoginEvent log) {				
				trainingForToday = log.getTreiningPath();	
				mainMenu.setTrainingForToday(trainingForToday);
				System.out.println("Lisener is set Training path is " + trainingForToday);
			}			
		});
		
		location = new int[2];
		addComponentListener(new ComponentAdapter() {
		    public void componentMoved(ComponentEvent e) {
//		    	parentLocation = getLocation();
//		    	x = parentLocation.x;
//				y = parentLocation.y;
				System.out.println("from adapter "+x + " " + y);
				
//				location[0] = x;
//				location[1] = y;
				setParentLocation();				
		    }
		});
		
		addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent e){
		        System.out.println("Mouse was clicked on my frame!");// test to remove		       
				loginDialog.setVisible(true);				
				loginDialog.getAuthorisationConfirmation();
		    }
		});
		
		
		System.out.println("this comes from frame" + getLocation());// test to remove
	
		
	// end of constructor - for orientation
	}
	
	
	
	
	//this method will pass current location to any component who need it
	public int[] setParentLocation(){
		
		parentLocation = getLocation();
    	x = parentLocation.x;
		y = parentLocation.y;
//		
		System.out.println(x + " " + y);
		int []location = {x, y};
		
		for(int i: location){
			System.out.print(i + ",");
		}
		System.out.println();
		viewTrainings = new ViewTreningsDialog();
		viewTrainings.setLocation(location);
		
		return location;
	}
	
	
	
	
	
	
	
	
}








