package GUI_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/*
 *  This class builds menu bar of this application
 */

public class ApplicationMenu extends JMenuBar implements ActionListener{

	private JMenu viewMenu, userMenu, helpMenu;
	private JMenuItem login, register,userRoster, userTreining, exit, createTraining, viewTrainings,  about;
	
	private CreateTreningsDialog creteTrainingFormDialog;
	private ViewTreningsDialog createViewTrainingDialog;
	private TrainingLoader trainingLoader;
	
	private LoginDialog loginDialog;
	private HashMap<Integer, String> personalTrainingDataBase;
	
	private String trainingForToday;
	
	private int[] dialogLocation;
	
	public ApplicationMenu(){		
		
	// 2 menu Quick access to current training whit option to adjustment
		userMenu = new JMenu("User");
		login = new JMenuItem("Log in to your accout");
		register = new JMenuItem("Create your own account");
		userTreining = new JMenuItem("Go steight to your treining");		
		exit = new JMenuItem("Exit");
		
		userMenu.add(login);
		userMenu.add(register);
		userMenu.add(userTreining);		
		userMenu.add(exit);	
		
		login.addActionListener(this);
		
		userTreining.addActionListener(this);
		exit.addActionListener(this);
		
	// 3 menu refers to training management
		viewMenu = new JMenu("View");		
		/* this item opens dialog where user can check out all existing 
		 training plan and use one of them */
		viewTrainings = new JMenuItem("View all trainings");
		viewMenu.add(viewTrainings);
		viewTrainings.addActionListener(this);
		
		// this item opens dialog where user can compose its own training plan
		createTraining = new JMenuItem("Create new Training");		
		viewMenu.add(createTraining);	
		createTraining.addActionListener(this);
			
		
		
	    // 4 menu refers to help and information option
		helpMenu = new JMenu("Help");
		about = new JMenuItem("About");	 // some info about application presented in a small dialog
		
		helpMenu.add(about);
		about.addActionListener(this);
		
		// menus added to menu bar
		add(userMenu);
		add(viewMenu);
		add(helpMenu);
	
		loginDialog = new LoginDialog();		
		loginDialog.setEntryPaneListener(new LoginListener(){
			public void loginEventOccured(LoginEvent log) {				
				trainingForToday = log.getTreiningPath();				
				System.out.println("Lisener is set Training path is " + trainingForToday);
			}
			
		});
		
	}
	
	
	// this method will check what item was choosen by user and take appropriate Action
	public void actionPerformed(ActionEvent itemEvent) {
		
			JMenuItem itemSelected = (JMenuItem) itemEvent.getSource();
			
			  if(itemSelected == login){
				    loginDialog.setVisible(true);				
					loginDialog.getAuthorisationConfirmation();
				  	
		}else if(itemSelected == createTraining){				  
					creteTrainingFormDialog = new CreateTreningsDialog();	
					creteTrainingFormDialog.setVisible(true);
					creteTrainingFormDialog.setModal(false);
		}else if(itemSelected == viewTrainings){
					createViewTrainingDialog = new ViewTreningsDialog();
					createViewTrainingDialog.setVisible(true);
					createViewTrainingDialog.setModal(false);
		}else if(itemSelected == userTreining){			
					loadTraining();
		}else if(itemSelected == exit){
					System.exit(0);
		}else if(itemSelected == about){
					JOptionPane.showMessageDialog(null, "Created by ziejaCODE");
		}
	}
	
	
	public void setTrainingForToday(String training){
		
		trainingForToday = training;
		
		
	}
	
	
	public void loadTraining(){
		
		System.out.println("What is " + trainingForToday);// test to remove
		
		if(trainingForToday != null){		
			TreningView treningView = new TreningView(trainingForToday);
			treningView.setVisible(true);
		}else{
			JOptionPane.showMessageDialog(null, "  This is user section. Please log in first");
		}
		
		
		
//		System.out.println(loginDialog.getUserFilePath());
		
		
	}
	
	public void setLocation(int[]location){
		
		dialogLocation = location;
		
	}
}












