package GUI_2;

import java.awt.HeadlessException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/*
 * this class is responsible for running Mainframe
 */

public class ApplicationRunner {

	public static void main(String[] args) {

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
	          public void run() {	              
	        	  try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					  MainFrame frame = new MainFrame();
					  frame.setVisible(true);
				} catch (HeadlessException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}	              
	          }
	      });
		
	}
	
}
