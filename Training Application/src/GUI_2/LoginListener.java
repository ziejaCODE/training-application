package GUI_2;

import java.util.EventListener;

public interface LoginListener extends EventListener {
	
	public void loginEventOccured(LoginEvent log);

}
