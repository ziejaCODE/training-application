package GUI_2;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginDialog extends JDialog{

	private JLabel titleLable;
	private JPanel mainPanel;
	private JTextField loginBox;
	private JPasswordField passwordBox;
	private JButton loginButton;
	private RegistrationDialog registerMe;
	
	private UserStatus userStatus;
	private String userFilePath;
	private static String usersPath;
	
	
	private LoginListener logListener;
	
	public LoginDialog(){
		
		// setting panel
		mainPanel = new JPanel();
		mainPanel.setSize(300,200);
		mainPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		
		//setting panel components
		titleLable = new JLabel("Please provide login details");
		gc.gridx = 0;
		gc.gridy = 0;
		mainPanel.add(titleLable, gc);
		
		loginBox = new JTextField(15);
		gc.gridx = 0;
		gc.gridy = 1;
		mainPanel.add(loginBox, gc);
		
		passwordBox = new JPasswordField(15);
		gc.gridx = 0;
		gc.gridy = 2;
		mainPanel.add(passwordBox, gc);
		
		loginButton = new JButton("Log in");
		gc.gridx = 0;
		gc.gridy = 3;
		mainPanel.add(loginButton, gc);
		
		// setting dialog
		setTitle("Login dialog");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setContentPane(mainPanel);
		setSize(new Dimension(mainPanel.getSize()));
		setLocationRelativeTo(null);
		
	}		
				
	public void getAuthorisationConfirmation(){
		
		loginButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// details are taken from the entering user
				usersPath = "users\\";
				String login = loginBox.getText();
				usersPath += login + "\\";
				String simgleUserPath = usersPath + login + ".txt";
				
				String password = passwordBox.getText(); // to correct 
				
				File usersFolder = new File(simgleUserPath);
				String[]loginFileContens = new String[2];
				
				// files checked
				if(usersFolder.exists()){
					System.out.println(usersPath + " and " + password);
					
					// scanner will read login file
					Scanner read = null;
					try {
						read = new Scanner(usersFolder);
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
						 
					}
					// read login file and put content into array
					for(int i = 0; i < 2; i++){
						loginFileContens[i] = read.nextLine();
						System.out.println("line " + i + " " + loginFileContens[i]);
					}
					// check password and grant access or ask to register
					if(loginFileContens[1].equals(password)){
						
						// **** here access to personal file after verification has been granted						
						userFilePath = usersPath + "Trenings\\" + checkDate();
						userStatus = new UserStatus();
						userStatus.setUsersStatus("y");
						// test to remove
						File f = new File(userFilePath);
						if(f.exists()){
							
							System.out.println("access granted to: " + userFilePath + " , user status is " + userStatus.getUsersStatus());// test to remove
						
							// Custom login event is created 
							LoginEvent logEv = new LoginEvent(this, userFilePath);
							
//														
							if(logListener != null){					
								logListener.loginEventOccured(logEv);
								System.out.println("oto path 1" + logEv.getTreiningPath());
								System.out.println("oto path 2" + usersPath);
								
							}else{
								System.out.println("Lisener is null");// test to remove
							}
						}else
							JOptionPane.showMessageDialog(null, "  You don't have any training loaded for today");	
						
						
						
						dispose();
						
					}else{
						System.out.println("false");
						userFilePath = null;
						JOptionPane.showMessageDialog(null, "Your passoword or login is incorrect");
					}
					
					
				}else{
					System.out.println("false");
					JOptionPane.showMessageDialog(null, "You don't have an accout");
					registerMe = new RegistrationDialog();
					registerMe.setVisible(true);
				}
				loginBox.setText("");
				passwordBox.setText("");
			}
		});
		
		
	}
	
	
	
	public String checkDate(){
		
				//find out what day of the week is today
				Date today = new Date();		
				String d = today.toString();		
				String[] words = d.split("\\ ");		
				String todayDay = words[0];
				
				System.out.println(d); // test to remove
				System.out.println(todayDay); // test to remove
				
		return 	todayDay;
	}
		
	
	// this method will be called by MainFrame for setting Listener 
	public  void setEntryPaneListener(LoginListener logListener){		
		this.logListener = logListener;
			
	}

	public String getUserPath() {
		return usersPath + "Trenings\\";
	}
	
	
}























